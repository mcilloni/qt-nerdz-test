// Copyright (C) 2017 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#include "nerdzwrapper.hh"

#include <QtGui>
#include <QtCore>
#include <QtNetworkAuth>

const QUrl hotUrl("https://api.nerdz.eu/v1/me/home");

NerdzWrapper::NerdzWrapper(QObject *parent) : QObject(parent)
{
    auto replyHandler = new QOAuthHttpServerReplyHandler(1337, this);
    oauth2.setReplyHandler(replyHandler);
    oauth2.setAuthorizationUrl(QUrl("https://api.nerdz.eu/v1/oauth2/authorize"));
    oauth2.setAccessTokenUrl(QUrl("https://api.nerdz.eu/v1/oauth2/token"));
    oauth2.setScope("base:read");

    connect(&oauth2, &QOAuth2AuthorizationCodeFlow::statusChanged, [=](
            QAbstractOAuth::Status status) {
        if (status == QAbstractOAuth::Status::Granted)
            emit authenticated();
    });
    
    oauth2.setModifyParametersFunction([&](QAbstractOAuth::Stage stage, QMultiMap<QString, QVariant> *parameters) {
        if (stage == QAbstractOAuth::Stage::RequestingAuthorization && isPermanent())
            parameters->insert("duration", "permanent");
    });
    connect(&oauth2, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser,
            &QDesktopServices::openUrl);
}

NerdzWrapper::NerdzWrapper(const QString &clientIdentifier, const QString &clientSecret, QObject *parent) :
    NerdzWrapper(parent)
{
    oauth2.setClientIdentifier(clientIdentifier);
    oauth2.setClientIdentifierSharedKey(clientSecret);
}

QNetworkReply *NerdzWrapper::requestHomePosts()
{
    qDebug() << "Getting homepage...";
    return oauth2.get(hotUrl);
}

bool NerdzWrapper::isPermanent() const
{
    return permanent;
}

void NerdzWrapper::setPermanent(bool value)
{
    permanent = value;
}

void NerdzWrapper::grant()
{
    oauth2.grant();
}
