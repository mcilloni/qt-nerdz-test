// Copyright (C) 2017 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#ifndef REDDITWRAPPER_H
#define REDDITWRAPPER_H

#include <QtCore>
#include <QtNetwork>

#include <QOAuth2AuthorizationCodeFlow>

class NerdzWrapper : public QObject
{
    Q_OBJECT

public:
    NerdzWrapper(QObject *parent = nullptr);
    NerdzWrapper(const QString &clientIdentifier, const QString &clientSecret, QObject *parent = nullptr);

    virtual ~NerdzWrapper() {}

    QNetworkReply *requestHomePosts();

    bool isPermanent() const;
    void setPermanent(bool value);

public slots:
    void grant();

signals:
    void authenticated();
    void subscribed(const QUrl &url);

private:
    QOAuth2AuthorizationCodeFlow oauth2;
    bool permanent = false;
};

#endif // REDDITWRAPPER_H
