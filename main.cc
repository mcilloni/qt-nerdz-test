// Copyright (C) 2017 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#include "nerdzmodel.hh"

#include <QtCore>
#include <QtWidgets>

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    QCommandLineParser parser;
    const QCommandLineOption clientId(QStringList() << "i" << "client-id",
                                      "Specifies the application client id", "client_id");
    
    const QCommandLineOption clientSecret(QStringList() << "s" << "client-secret",
                                      "Specifies the application client shared secret", "client_secret");

    parser.addOptions({ clientId, clientSecret });

    parser.process(app);

    if (parser.isSet(clientId) && parser.isSet(clientSecret)) {
        const auto cl_id { parser.value(clientId) };
        const auto cl_secr { parser.value(clientSecret) };

        QListView view {};

        NerdzModel model(cl_id, cl_secr);
        view.setModel(&model);
        view.show();
        return app.exec();
    } else {
        parser.showHelp();
    }

    return 0;
}
