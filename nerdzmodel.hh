// Copyright (C) 2017 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#ifndef REDDITMODEL_H
#define REDDITMODEL_H

#include "nerdzwrapper.hh"

#include <QtCore>

QT_FORWARD_DECLARE_CLASS(QNetworkReply)

class NerdzModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    NerdzModel(QObject *parent = nullptr);
    NerdzModel(const QString &clientId, const QString &clientSecret, QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    void grant();

signals:
    void error(const QString &errorString);

private slots:
    void update();

private:
    NerdzWrapper nerdzWrapper;
    QPointer<QNetworkReply> liveThreadReply;
    QList<QJsonObject> threads;
};

#endif // REDDITMODEL_H
