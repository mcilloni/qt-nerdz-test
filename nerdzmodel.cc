// Copyright (C) 2017 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#include "nerdzmodel.hh"

#include <QtCore>
#include <QtNetwork>

NerdzModel::NerdzModel(QObject *parent) : QAbstractTableModel(parent) {}

NerdzModel::NerdzModel(const QString &clientId, const QString &clientSecret, QObject *parent) :
    QAbstractTableModel(parent),
    nerdzWrapper(clientId, clientSecret)
{
    grant();
}

int NerdzModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return threads.size();
}

int NerdzModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return threads.size() ? 1 : 0;
}

QVariant NerdzModel::data(const QModelIndex &index, int role) const
{
    Q_UNUSED(role);
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DisplayRole) {
        const auto childrenObject = threads.at(index.row());

        const auto fromValue { childrenObject.value("from") };
        const auto messageValue { childrenObject.value("message") };        
        const auto toValue { childrenObject.value("to") };

        Q_ASSERT(fromValue.isObject());
        Q_ASSERT(messageValue.isString());
        Q_ASSERT(toValue.isObject());

        const auto from { fromValue.toObject() };
        const auto msg { messageValue.toString() };
        const auto to { toValue.toObject() };

        const auto sendValue { from.value("username") };
        Q_ASSERT(sendValue.isString());

        const auto sender { sendValue.toString() };

        QString txt {};

        if (from.value("board") == to.value("board")) {
            txt = QString("%1: %2").arg(sender, msg);
        } else {
            const auto recvValue { to.value("username") };
            Q_ASSERT(recvValue.isString());

            const auto recv { recvValue.toString() };

            txt = QString("%1 -> %2: %3").arg(sender, recv, msg);
        }

        return txt;
    }
    
    return QVariant();
}

void NerdzModel::grant()
{
    nerdzWrapper.grant();
    connect(&nerdzWrapper, &NerdzWrapper::authenticated, this, &NerdzModel::update);
}

void NerdzModel::update()
{
    auto reply = nerdzWrapper.requestHomePosts();

    connect(reply, &QNetworkReply::finished, [=]() {
        reply->deleteLater();
        if (reply->error() != QNetworkReply::NoError) {
            emit error(reply->errorString());
            return;
        }
        const auto json = reply->readAll();
        const auto document = QJsonDocument::fromJson(json);

        Q_ASSERT(document.isObject());
        const auto rootObject = document.object();

        const auto dataValue = rootObject.value("data");
        Q_ASSERT(dataValue.isArray());

        const auto dataArray = dataValue.toArray();

        if (dataArray.isEmpty()) {
            return;
        }

        beginInsertRows(QModelIndex(), threads.size(), dataArray.size() + threads.size() - 1);
        for (const auto childValue : qAsConst(dataArray)) {
            Q_ASSERT(childValue.isObject());
            threads.append(childValue.toObject());
        }
        endInsertRows();
    });
}
